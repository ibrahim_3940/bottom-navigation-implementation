package com.demo.bottomnavigationexample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    //defining globally to avoid commiting transactions again and again, we will just hide and show then
    val homeFragment: Fragment = HomeFragment()
    val dadhboardFragment: Fragment = DashboardFragment()
    val notificationFragment: Fragment = NotificationFragment()
    val fm: FragmentManager = supportFragmentManager
    var active: Fragment = homeFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        commitAllTransactions()

    }

    fun commitAllTransactions() {

        fm.beginTransaction().add(R.id.main_container, notificationFragment, "3")
            .hide(notificationFragment).commit()
        fm.beginTransaction().add(R.id.main_container, dadhboardFragment, "2")
            .hide(dadhboardFragment).commit()
        fm.beginTransaction().add(R.id.main_container, homeFragment, "1").commit()
    }

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    fm.beginTransaction().hide(active).show(homeFragment).commit()
                    active = homeFragment
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_dashboard -> {
                    fm.beginTransaction().hide(active).show(dadhboardFragment).commit()
                    active = dadhboardFragment
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_notifications -> {
                    fm.beginTransaction().hide(active).show(notificationFragment).commit()
                    active = notificationFragment
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

}